from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sklad_knih.views.home', name='home'),
    # url(r'^sklad_knih/', include('sklad_knih.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^titul_zanr$', 'sklad_knih.modely.views.titul_zanr'),
    url(r'^dodavatele/(?P<id_dodavatel>\d+)$', 'sklad_knih.modely.views.dodavatel'),
    url(r'^autor/$', 'sklad_knih.modely.views.autor'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'sklad_knih.modely.views.index'),
    url(r'^tituly/', 'sklad_knih.modely.views.tituly'),
    url(r'^dodavatele/$', 'sklad_knih.modely.views.dodavatele'),
    url(r'^autori/$', 'sklad_knih.modely.views.autori'),

)
