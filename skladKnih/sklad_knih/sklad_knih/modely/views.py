# Create your views here.
from django.shortcuts import get_object_or_404, render_to_response
from sklad_knih.modely.models import Titul, Dodavatel, Autor
from forms import TitulForm, DodavatelForm, JinyAutorForm
from django.core.context_processors import csrf

def index(request):
    seznam_dodavatelu = Dodavatel.objects.all().order_by('jmeno')
    return render_to_response('index.html', {'seznam_dodavatelu' : seznam_dodavatelu})

#vraci vsechny tituly serazene podle abecedy
def tituly(request):
    seznam_titulu = Titul.objects.all().order_by('nazev')
    return render_to_response('tituly.html', {'seznam_titulu' : seznam_titulu})

#vypise vsechny dodavatele serazene podle abecedy
def dodavatele(request):
    seznam_dodavatelu = Dodavatel.objects.all().order_by('jmeno')
    seznam_titulu = Titul.objects.all().order_by('dodavatel')
    return render_to_response('dodavatele.html', {'seznam_dodavatelu' : seznam_dodavatelu, 'seznam_titulu' : seznam_titulu})

#vypise vsechny autory serazene podle abecedy
def autori(request):
    seznam_autoru = Autor.objects.all().order_by('jmeno')
    seznam_titulu = Titul.objects.all().order_by('dodavatel')
    return render_to_response('autori.html', {'seznam_autoru' : seznam_autoru, 'seznam_titulu' : seznam_titulu})


#vyhledava tituly podle zanru
def titul_zanr(request):
    c = {}
    c.update(csrf(request))
    z = {}
    if request.method == 'POST':
        #vytvoreni formulare naplneneho poslanymi daty
        zanr = TitulForm(request.POST)
        if zanr.is_valid():
            # ulozi zadany nazev zanru do promenne z a smaze formular
            z = zanr.cleaned_data['zanr']
            #vyfiltruje vsechny tituly zadaneho zanru
            titul = Titul.objects.filter(zanr=z).order_by('nazev')
            z = zanr
        else:
            titul = None
    else:
        #vytvoreni objektu typu formular s vychozimi hodnotami
        z = TitulForm({'zanr': 'programovani'})
        titul = None
    c['z'] = z
    c['seznam_titulu'] = titul
    return render_to_response('titul_zanr.html', c)

#meni detaily konkretniho dodavatele
def dodavatel(request, id_dodavatel):
    #pracuji jen s dodavatelem, ktery je vybran pomoci url, jinak vracim 404
    dodavatel = get_object_or_404(Dodavatel, pk = id_dodavatel)
    c = {}
    c.update(csrf(request))
    tel = 1
    if request.method == 'POST':
        #vytvoreni formulare naplneneho poslanymi daty
        dod_form = DodavatelForm(request.POST)
        if dod_form.is_valid():
            # ulozi zadany nazev zanru do promenne z a smaze formular
            tel = dod_form.cleaned_data['telefon']
            adr = dod_form.cleaned_data['adresa']
            #overeni telefonniho cisla, spatny se neulozi
            if tel > 99999999 and tel < 1000000000:
                #ulozeni do databaze jen kdyz je tel. cislo overeno
                #nove hodnoty namisto starych
                dodavatel.adresa = adr
                dodavatel.telefon = tel
                dodavatel.save()
            else:
                tel = 0
            #vyplneni formulare daty z databaze
            dod_form = DodavatelForm({'adresa': dodavatel.adresa, 'telefon': dodavatel.telefon})
            
        #kdyz je zadana dlouha adresa, znaky misto cisel v tel. cisle nebo jina necekana chyba
        else:
            dod_form = DodavatelForm({'adresa': dodavatel.adresa, 'telefon': dodavatel.telefon})
            tel = 0
    else:
        #vytvoreni objektu typu formular s vychozimi hodnotami
        dod_form = DodavatelForm({'adresa': dodavatel.adresa, 'telefon': dodavatel.telefon})

    c['dodavatel'] = dodavatel
    c['dod_form'] = dod_form
    #promenna pouze pro vyskoceni chybove hlasky v pripade chybne zadaneho tel. cisla
    c['tel'] = tel
    c['seznam_titulu'] = Titul.objects.all().order_by('dodavatel')
    return render_to_response('dodavatel.html', c)

#Vypisa detaily o urcitem autorovi
def autor(request):
    c = {}
    c.update(csrf(request))
    autor = {}
    if request.method == 'POST':
        #vytvoreni formulare naplneneho poslanymi daty
        ja_form = JinyAutorForm(request.POST)
        if ja_form.is_valid():
            autor = ja_form.cleaned_data['autor']
            #pokud je chybne zvoleno prijmeni autora, pak je vracen kod 404
            autor = get_object_or_404(Autor, prijmeni = autor)
        else:
            autor = None
    else:
        ja_form = JinyAutorForm({'autor' : "Zvol si autora"})
    c['autor'] = autor
    c['ja_form'] = ja_form
    c['seznam_titulu'] = Titul.objects.all().order_by('autor')
    
    return render_to_response('autor.html', c)