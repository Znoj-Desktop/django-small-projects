from django import forms

class TitulForm(forms.Form):
    zanr = forms.CharField(label = 'Zanr:', max_length=30)

class DodavatelForm(forms.Form):
    adresa = forms.CharField(label = 'Adresa:', max_length=50)
    telefon = forms.IntegerField(label = 'Telefon:')
    
class JinyAutorForm(forms.Form):
    autor = forms.CharField(label = 'Prijmeni:', max_length=30)