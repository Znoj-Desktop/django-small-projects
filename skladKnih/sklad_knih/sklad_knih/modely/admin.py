from sklad_knih.modely.models import Titul, Dodavatel, Autor
from django.contrib import admin

admin.site.register(Titul)
admin.site.register(Dodavatel) 
admin.site.register(Autor)