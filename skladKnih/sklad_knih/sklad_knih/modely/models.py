from django.db import models

# Create your models here.
#3 modely - Titul, Dodavatel, Autor
class Titul(models.Model):
    nazev = models.CharField(max_length=50, unique=True)
    rok = models.IntegerField()
    kusu = models.IntegerField()
    zanr = models.CharField(max_length=30)
    nakladatelstvi = models.CharField(max_length=30)
    dodavatel = models.ForeignKey('Dodavatel')
    autor = models.ForeignKey('Autor')

    #reprezentace objektu pro prehlednost a pro pouziti v generovanem administracnim rozhrani
    def __unicode__(self):
        return self.nazev

class Dodavatel(models.Model):
    jmeno = models.CharField(max_length=30, unique=True)
    adresa = models.CharField(max_length=50)
    telefon = models.IntegerField()
    
    #reprezentace objektu pro prehlednost a pro pouziti v generovanem administracnim rozhrani
    def __unicode__(self):
        return self.jmeno

class Autor(models.Model):

    prijmeni = models.CharField(max_length=30)
    jmeno = models.CharField(max_length=30)
    rok_narozeni = models.IntegerField()
    rok_umrti = models.IntegerField(null=True)
    narodnost = models.CharField(max_length=30)
    
    #reprezentace objektu pro prehlednost a pro pouziti v generovanem administracnim rozhrani
    def __unicode__(self):
        return self.prijmeni
