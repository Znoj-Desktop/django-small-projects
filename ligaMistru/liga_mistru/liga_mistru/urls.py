from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lecebna.views.home', name='home'),
    # url(r'^lecebna/', include('lecebna.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tymy/', 'liga_mistru.models.views.tymy'),
    url(r'^hraci/', 'liga_mistru.models.views.hraci'),
    url(r'^zapasy/', 'liga_mistru.models.views.zapasy'),
    url(r'^hraci_tymu/', 'liga_mistru.models.views.hraci_tymu'),
    url(r'^zapasy_tymu/', 'liga_mistru.models.views.zapasy_tymu'),
    url(r'^hraci_vek/', 'liga_mistru.models.views.hraci_vek'),
    
)
