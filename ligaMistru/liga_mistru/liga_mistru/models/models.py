from django.db import models

# Create your models here.
class Hrac(models.Model):
    # atribut jmeno ma maximalni delku 30 znaku
    jmeno = models.CharField(max_length=30)
    prijmeni = models.CharField(max_length=10)
    cislo_dresu = models.IntegerField()
    vek = models.IntegerField()
    #odkaz na tabulku tym = tym ve kterem hrac hraje
    tym = models.ForeignKey('Tym')
    
    #metoda urcuje, jak se bude "tvarit" trida na venek - jak bude reprezentovana.
    #konkretni trida bude reprezentovana prijmenim hrace
    def __unicode__(self):
        return self.prijmeni
    
class Tym(models.Model):
    # atribut nazev je unikatni a ma maximalni delku 30 znaku
    nazev = models.CharField(max_length=30, unique = True)
    poradi = models.IntegerField()
    stat = models.CharField(max_length=30)

    #tym bude vystupovat pod svym nazvem a ne pod pridelenym id
    def __unicode__(self):
        return self.nazev

class Zapas(models.Model):
    # atribut mesto je unikatni a ma maximalni delku 30 znaku
    mesto = models.CharField(max_length=30)
    datum = models.DateField()
    domaci = models.ForeignKey('Tym', related_name='d')
    hoste = models.ForeignKey('Tym', related_name='h')
    #atributy mohou byt null, to je pripad, kdy se jeste zapas neodehral
    vitez = models.ForeignKey('Tym', null=True, blank=True, related_name='v')
    vysledek = models.CharField(max_length=30, blank=True, null=True)
    #zapas je reprezentovan datumem, domacim a hostujicim tymem 
    def __unicode__(self):
        return str(self.datum.strftime("%Y-%m-%d") + " - " + self.domaci.nazev + " vs. " + self.hoste.nazev)