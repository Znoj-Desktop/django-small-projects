from liga_mistru.models.models import Tym, Hrac, Zapas
from django.contrib import admin

admin.site.register(Tym)
admin.site.register(Hrac) 
admin.site.register(Zapas)