from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lecebna.views.home', name='home'),
    # url(r'^lecebna/', include('lecebna.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^oddeleni/', 'lecebna.models.views.oddeleni'),
    url(r'^pacienti/', 'lecebna.models.views.pacienti'),
    url(r'^doktori/', 'lecebna.models.views.doktori'),
    url(r'^oddeleni_pocet/', 'lecebna.models.views.oddeleni_pocet'),
    url(r'^pacienti_oddeleni/', 'lecebna.models.views.pacienti_oddeleni'),
    url(r'^pacienti_doktora/', 'lecebna.models.views.pacienti_doktora'),
    
)
