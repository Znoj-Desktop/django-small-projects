### **Description**

Django small projects

---

### **Technology**

Python

---

### **Year**

2012

---

## **Léčebna**

### **Diagram**

![](./README/lecebnaDiagram.png)

---

### **Tables**

![](./README/lecebnaTabulky.jpg)

---

## **Liga mistrů**

### **Diagram**

![](./README/ligaDiagram.png)

---

### **Tables**

![](./README/ligaTabulky.png)
![](./README/ligaTabulkyAtributy.png)

---

## **Nemocnice**

---

## **Sklad knih**

---

## **Zápasy**

---

### **Diagram**

![](./README/zapasyDiagram.png)

---

### **Tables**

![](./README/zapasyTabulky.png)

---

## **Zkoušky**

---

### **Tables**

![](./README/zkouskyTabulky.png)

---
