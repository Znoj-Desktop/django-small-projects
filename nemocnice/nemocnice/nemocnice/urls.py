from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'nemocnice.views.home', name='home'),
    # url(r'^nemocnice/', include('nemocnice.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^oddeleni/', 'nemocnice.models.views.oddeleni'),
    url(r'^pacienti/', 'nemocnice.models.views.pacienti'),
    url(r'^doktori/', 'nemocnice.models.views.doktori'),
    url(r'^oddeleni_pocet/', 'nemocnice.models.views.oddeleni_pocet'),
    url(r'^pacienti_oddeleni/', 'nemocnice.models.views.pacienti_oddeleni'),
    url(r'^pacienti_doktora/', 'nemocnice.models.views.pacienti_doktora'),
    
)
