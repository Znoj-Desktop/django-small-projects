from django import forms

class OddPocForm(forms.Form):
    odd_form = forms.CharField(label = 'Oddeleni:', max_length=30)

class PacOddForm(forms.Form):
    odd_form = forms.CharField(label = 'Oddeleni:', max_length=30)
    
class PacDokForm(forms.Form):
    doktor_form = forms.CharField(label = 'Prijmeni doktora:', max_length=30)