from django.db import models

# Create your models here.
class Oddeleni(models.Model):
    # atribut jmenoOddeleni je unikatni a ma maximalni delku 30 znaku
    jmeno_oddeleni = models.CharField(max_length=30, unique = True)
    mistnost = models.CharField(max_length=10)
    tel_oddeleni = models.IntegerField()
    #metoda urcuje, jak se bude "tvarit" trida na venek - jak bude reprezentovana.
    #konkretni trida bude reprezentovana jmenem oddeleni
    def __unicode__(self):
        return self.jmeno_oddeleni
    
class Doktor(models.Model):
    # atribut prijmeniDoktora je unikatni a ma maximalni delku 30 znaku
    prijmeni_doktora = models.CharField(max_length=30, unique = True)
    jmeno_doktora = models.CharField(max_length=30)
    titul = models.CharField(max_length=20)
    adresa_doktora = models.CharField(max_length=30)
    tel_doktora = models.IntegerField()
    #vazba pomoci ciziho klice na oddeleni ve kterem doktor pracuje.
    #Doktor pracuje v prave jednom oddeleni
    oddeleni = models.ForeignKey('Oddeleni')
    
    def __unicode__(self):
        return self.prijmeni_doktora

#kazdy pacient ma nejakeho opatrovnika, na nej uchovavame v tabulce telefon a jeho jmeno
class Pacient(models.Model):
    # atribut prijmeniDoktora je unikatni a ma maximalni delku 30 znaku
    prijmeni_pacienta = models.CharField(max_length=30, unique = True)
    jmeno_pacienta = models.CharField(max_length=30)
    adresa_pacienta = models.CharField(max_length=30)
    tel_pacienta = models.IntegerField()
    #jmeno opatrovnika je i s prijmenim
    jmeno_opatrovnika = models.CharField(max_length=60, null = True)
    tel_opatrovnika = models.IntegerField(null = True)
    #vazba pomoci ciziho klice na oddeleni a doktora
    doktor = models.ForeignKey('Doktor')
    
    def __unicode__(self):
        return self.prijmeni_pacienta