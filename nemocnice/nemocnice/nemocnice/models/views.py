# Create your views here.
from django.shortcuts import get_object_or_404, render_to_response
from django.core.context_processors import csrf
from nemocnice.models.models import Oddeleni, Pacient, Doktor
from forms import OddPocForm, PacOddForm, PacDokForm

#vypise vsechny oddeleni
def oddeleni(request):
    #vytahnu si vsechna oddeleni z db a seradim je podle jejich jmena
    vsechna_oddeleni = Oddeleni.objects.all().order_by('jmeno_oddeleni')
    #vracim slovnik se vsemi oddelenimi a zobrazuju to podle tepmlatu oddeleni.thml
    return render_to_response('oddeleni.html', {'vsechna_oddeleni' : vsechna_oddeleni})
   
#vypise vsechny pacienty
def pacienti(request):
    #vytahnu si z db seznam vsech pacientu a seradim je podle abecedy podle prijmeni pacienta
    vsichni_pacienti = Pacient.objects.all().order_by('prijmeni_pacienta')
    #je potreba poslat i vsechny doktory, abychom mohli vypsat jmeno doktora, ktery se o daneho pacienta stara
    vsichni_doktori = Doktor.objects.all()
    return render_to_response('pacienti.html', {'vsichni_pacienti' : vsichni_pacienti, 'vsichni_doktori' : vsichni_doktori})

#vypise vsechny doktory
def doktori(request):
    #vytahnu si z db seznam vsech doktoru a seradim je podle abecedy podle prijmeni doktora
    vsichni_doktori = Doktor.objects.all().order_by('prijmeni_doktora')
    #vytahnu si vsechna oddeleni, abych mohl vypsat pro kazdeho doktora oddeleni do ktereho patri
    vsechna_oddeleni = Oddeleni.objects.all().order_by('jmeno_oddeleni')
    return render_to_response('doktori.html', {'vsichni_doktori' : vsichni_doktori, 'vsechna_oddeleni' : vsechna_oddeleni})

#vypise pocet doktoru na zvolenem oddeleni
def oddeleni_pocet(request):
    slovnik = {}
    slovnik.update(csrf(request))
    data = ""
    #podminka, pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = OddPocForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu
        if formular.is_valid():
            #do promennedata jsou ulozena data z formulare (z formulare v promenne formular)
            data = formular.cleaned_data['odd_form']
            #do slovniku pod klic oddeleni je ulozeno pozadovane oddeleni z tridy Oddeleni,
            #nebo je vracen kod 404
            slovnik['oddeleni'] = get_object_or_404(Oddeleni, jmeno_oddeleni = data)
            #ulozime do slovniku pocet vsech doktoru daneho oddeleni
            slovnik['doktori'] = Doktor.objects.filter(oddeleni_id = slovnik['oddeleni'].id)
            slovnik['pocet_doktoru'] = slovnik['doktori'].count()
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = OddPocForm({'odd_form' : 'Zvol si oddeleni'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular    
        formular = OddPocForm({'odd_form' : 'Zvol si oddeleni'})
            
    #ulozime do slovniku formular
    slovnik['formular'] = formular
    #vraci slovnik 'slovnik' do templatu oddeleni_pocet.html)
    return render_to_response('oddeleni_pocet.html', slovnik)

#vypise vsechny pacienty na zvolenem oddeleni
def pacienti_oddeleni(request):
    slovnik = {}
    slovnik.update(csrf(request))
    data = ""
    #podminka, pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = PacOddForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu
        if formular.is_valid():
            #do promennedata jsou ulozena data z formulare (z formulare v promenne formular)
            data = formular.cleaned_data['odd_form']
            #do slovniku pod klic oddeleni je ulozeno pozadovane oddeleni z tridy Oddeleni,
            #nebo je vracen kod 404
            slovnik['oddeleni'] = get_object_or_404(Oddeleni, jmeno_oddeleni = data)
            #ulozime do slovniku pocet vsech doktoru daneho oddeleni
            slovnik['doktori'] = Doktor.objects.filter(oddeleni_id = slovnik['oddeleni'].id)
            #ulozime do slovniku pocet vsech pacientu daneho oddeleni
            seznam_pacientu = []
            for doktor in slovnik['doktori']:
                seznam_pacientu += Pacient.objects.filter(doktor_id = doktor.id).order_by('prijmeni_pacienta')
            slovnik['seznam_pacientu'] = seznam_pacientu   
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = PacOddForm({'odd_form' : 'Zvol si oddeleni'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular    
        formular = PacOddForm({'odd_form' : 'Zvol si oddeleni'})
            
    #ulozime do slovniku formular
    slovnik['formular'] = formular
    #vraci slovnik 'slovnik' do templatu pacienti_oddeleni.html)
    return render_to_response('pacienti_oddeleni.html', slovnik)
    

#vypisou se vsichni pacienti zvoleneho doktora
def pacienti_doktora(request):
    slovnik = {}
    slovnik.update(csrf(request))
    doktor = ""
    slovnik['doktor'] = ""
    #podminka, pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = PacDokForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu
        if formular.is_valid():
            #do promenne doktor je ulozeno prijmeni doktora z formulare (z formulare v promenne formular)
            doktor = formular.cleaned_data['doktor_form']
            #do slovniku pod klic oddeleni je ulozeno pozadovane oddeleni z tridy Oddeleni,
            #nebo je vracen kod 404
            slovnik['doktor'] = get_object_or_404(Doktor, prijmeni_doktora = doktor)
            #ulozime do slovniku pocet vsech pacientu, kteri maji zvoleneho doktora
            slovnik['pacienti'] = Pacient.objects.filter(doktor_id = get_object_or_404(Doktor, prijmeni_doktora = doktor).id)
    
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = PacDokForm({'doktor_form' : 'Zvol si doktora'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular    
        formular = PacDokForm({'doktor_form' : 'Zvol si doktora'})
            
    #ulozime do slovniku formular
    slovnik['formular'] = formular
   
    #vraci slovnik 'slovnik' do templatu pacienti_doktora.html)
    return render_to_response('pacienti_doktora.html', slovnik)
