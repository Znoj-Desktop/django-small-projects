from nemocnice.models.models import Oddeleni, Pacient, Doktor
from django.contrib import admin

admin.site.register(Oddeleni)
admin.site.register(Pacient) 
admin.site.register(Doktor)