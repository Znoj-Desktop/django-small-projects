from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lecebna.views.home', name='home'),
    # url(r'^lecebna/', include('lecebna.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', 'zapasy.models.views.menu'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tymy/', 'zapasy.models.views.tymy'),
    url(r'^hraci/', 'zapasy.models.views.hraci'),
    url(r'^zapasy/', 'zapasy.models.views.zapasy'),
    url(r'^hraci_tymu/', 'zapasy.models.views.hraci_tymu'),
    url(r'^zapasy_tymu/', 'zapasy.models.views.zapasy_tymu'),
    url(r'^hraci_vek/', 'zapasy.models.views.hraci_vek'),
)
