from django import forms

class HracTymForm(forms.Form):
    tym_form = forms.CharField(label = 'Tym:', max_length=50)

class ZapasTymForm(forms.Form):
    tym_form = forms.CharField(label = 'Tym:', max_length=30)
    
class HracVekForm(forms.Form):
    min_form = forms.IntegerField(label = 'Minimalni vek:')
    max_form = forms.IntegerField(label = 'Maximalni vek:')