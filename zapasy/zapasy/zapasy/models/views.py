# Create your views here.
from zapasy.models.models import Tym, Hrac, Zapas
from django.core.context_processors import csrf
from django.shortcuts import render_to_response,get_object_or_404
from forms import HracTymForm, ZapasTymForm, HracVekForm

def menu(request):
    return render_to_response('menu.html')
    
    
#vypise vsechny tymy
def tymy(request):
    #vytahnu si vsechny tymy z db a seradim je podle jejich jmena
    vsechny_tymy = Tym.objects.all().order_by('nazev')
    #budu vypisovat pro kazdy tym taky vsechny jeho hrace...
    vsichni_hraci = Hrac.objects.all().order_by('prijmeni')
    #vracim slovnik se vsemi tymi a zobrazuju to podle tepmlatu tymy.thml
    return render_to_response('tymy.html', {'vsechny_tymy' : vsechny_tymy, 'vsichni_hraci' : vsichni_hraci})
   
#vypise vsechny hrace
def hraci(request):
    #vytahnu si z db seznam vsech hracu a seradim je podle abecedy podle prijmeni hrace
    vsichni_hraci = Hrac.objects.all().order_by('prijmeni')
    return render_to_response('hraci.html', {'vsichni_hraci' : vsichni_hraci})

#vypise vsechny zapasy
def zapasy(request):
    #vytahnu si z db seznam vsech zapasu
    vsechny_zapasy = Zapas.objects.all().order_by('datum')
    return render_to_response('zapasy.html', {'vsechny_zapasy' : vsechny_zapasy})

#vypise vsechny hrace ze zvoleneho tymu
def hraci_tymu(request):
    ht = {}
    ht.update(csrf(request))
    data = ""
    #podminka, pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = HracTymForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu, tak:...
        if formular.is_valid():
            #do promennedata jsou ulozena data z formulare (z formulare v promenne formular)
            data = formular.cleaned_data['tym_form']
            #do slovniku pod klic tym je ulozen pozadovany tym z tridy tym,
            #nebo je vracen kod 404
            ht['tym'] = get_object_or_404(Tym, nazev = data)
            #ulozime do slovniku vsechny hrace, kteri patri do daneho tymu
            ht['hraci'] = Hrac.objects.filter(tym_id = ht['tym'].id)
             
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = HracTymForm({'tym_form' : 'Zvol si tym, jehoz hrace chces nechat vypsat'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular    
        formular = HracTymForm({'tym_form' : 'Zvol si tym, jehoz hrace chces nechat vypsat'})
            
    #ulozime do slovniku formular
    ht['formular'] = formular
    #vraci slovnik 'ht' do templatu hraci_tymu.html)
    return render_to_response('hraci_tymu.html', ht)

#vypise vsechny zapasy ze zvoleneho tymu
def zapasy_tymu(request):
    zt = {}
    zt.update(csrf(request))
    data = ""
    #podminka, pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = ZapasTymForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu, tak:...
        if formular.is_valid():
            #do promennedata jsou ulozena data z formulare (z formulare v promenne formular)
            data = formular.cleaned_data['tym_form']
            #do slovniku pod klic tym je ulozen pozadovany tym z tridy Tym,
            #nebo je vracen kod 404
            zt['tym'] = get_object_or_404(Tym, nazev = data)
            #ulozime do slovniku vsechny hrace, kteri patri do daneho tymu
            zt['zapasy_d'] = Zapas.objects.filter(domaci_id = zt['tym'].id)
            zt['zapasy_h'] = Zapas.objects.filter(hoste_id = zt['tym'].id)
             
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = ZapasTymForm({'tym_form' : 'Zvol si tym'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular    
        formular = ZapasTymForm({'tym_form' : 'Zvol si tym'})
            
    #ulozime do slovniku formular
    zt['formular'] = formular
    #vraci slovnik 'ht' do templatu hraci_tymu.html)
    return render_to_response('zapasy_tymu.html', zt)

#vypise vsechny zapasy ze zvoleneho tymu
def hraci_vek(request):
    hv = {}
    hv.update(csrf(request))
    #podminka, pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = HracVekForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu, tak:...
        if formular.is_valid():
            #do promennedata jsou ulozena data z formulare (z formulare v promenne formular)
            data_min = formular.cleaned_data['min_form']
            data_max = formular.cleaned_data['max_form']
            #do slovniku pod klic hraci jsou ulozeni hraci, kde jejich min. vek je vetsi jak zadany vek v data_min
            #a maximalni je nizsi jak vek zadan v data_max
            hv['hraci'] = Hrac.objects.filter(vek__gte=data_min).filter(vek__lte=data_max).order_by('prijmeni')
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = HracVekForm({'min_form' : 0, 'max_form' : 0})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular    
        formular = HracVekForm({'min_form' : 0, 'max_form' : 0})
            
    #ulozime do slovniku formular
    hv['formular'] = formular
    #vraci slovnik 'ht' do templatu hraci_tymu.html)
    return render_to_response('hraci_vek.html', hv)
