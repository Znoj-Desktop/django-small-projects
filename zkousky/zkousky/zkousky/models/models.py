from django.db import models

# Create your models here.
#vsechny tridy dedi ze tridy models.Model
class Ucitel(models.Model):
    #primarni klic -> proto dam atribut UNIQUE=True
    #max_length je maximalni pocet znaku v tomto poli znaku - je nutne uvest
    #pro typ CharField
    login = models.CharField(max_length=10, unique = True)
    prijmeni = models.CharField(max_length=30)
    # atribut jmeno ma maximalni delku 30 znaku
    jmeno = models.CharField(max_length=30)
    email = models.CharField(max_length=30)

    #kazdy ucitel uci prave jeden predmet - ten ktery je zde reprezentovan cizim klicem
    #"predmet" odkazujicim na tabulku Predmet = predmet ktery tento ucitel uci
    #predmet nemusi byt zadan, aby se dala data snadneji zadavat. nejprve se muze do
    #databaze uvest ucitel, pak predmet a pak teprve se muze ucitel na predmet "navazat"
    predmet = models.ForeignKey('Predmet', blank=True, null=True)
    
    #metoda urcuje, jak se bude "tvarit" trida na venek - jak bude reprezentovana.
    #konkretni trida bude reprezentovana prijmenim ucitele
    def __unicode__(self):
        return self.prijmeni
    
class Predmet(models.Model):
    #primarni klic predmetu je zkratka (max 5 znaku)
    zkratka = models.CharField(max_length=5, unique = True)
    nazev = models.CharField(max_length=50)
    kredity = models.IntegerField()
    #kazdy predmet musi mit jednoho garanta
    garant = models.ForeignKey('Ucitel', related_name="garant")
    #zde jsou 3 terminy zkousek, kazdy predmet ma jen jeden prvni(2./3.) termin
    prvni_termin = models.ForeignKey('Zkouska', related_name="prvni")
    druhy_termin = models.ForeignKey('Zkouska', related_name="druhy")
    treti_termin = models.ForeignKey('Zkouska', related_name="treti")
    
    #instance teto tridy bude reprezentovana nazvem predmetu
    def __unicode__(self):
        return self.nazev
    
class Zkouska(models.Model):
    #primarni klic neuvadim -> vytvori se sam a kazde zkousce bude prideleno id
    mistnost = models.TextField(max_length=5)
    kapacita = models.IntegerField()
    datum = models.DateField()
    cas = models.TimeField()
    
    #instance teto tridy se bude na venek ukazovat jako "datum cas - mistnost"
    def __unicode__(self):
        return str(self.datum.strftime("%Y-%m-%d") + " " + self.cas.strftime("%H-%M") + " - " + self.mistnost)