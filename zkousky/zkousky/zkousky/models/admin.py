from zkousky.models.models import Predmet, Zkouska, Ucitel
from django.contrib import admin

admin.site.register(Predmet)
admin.site.register(Zkouska) 
admin.site.register(Ucitel)