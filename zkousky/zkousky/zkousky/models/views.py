# Create your views here.
from zkousky.models.models import Zkouska, Predmet, Ucitel
from django.core.context_processors import csrf
from django.shortcuts import render_to_response,get_object_or_404
from forms import upForm, zpForm

#vypise vsechny tymy
def zkousky(request):
    #vytahnu si vsechny zkousky z db a seradim je podle datumu
    zk = Zkouska.objects.all().order_by('datum')
    #vracim slovnik se vsemi zkouskami a zobrazuju to podle tepmlatu zkousky.html
    return render_to_response('zkousky.html', {'zk' : zk})

def predmety(request):
    #vytahnu si vsechny predmety z db a seradim je podle abecedy
    pr = Predmet.objects.all().order_by('nazev')
    #vracim slovnik se vsemi predmety a zobrazuju to podle tepmlatu predmety.html
    return render_to_response('predmety.html', {'pr' : pr})

def ucitele(request):
    #... vsechny ucitele podle prijmeni
    uc = Ucitel.objects.all().order_by('prijmeni')
    return render_to_response('ucitele.html', {'uc' : uc})

def ucitele_predmetu(request):
    up = {}
    up.update(csrf(request))
    #podminka - pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = upForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu, tak:...
        if formular.is_valid():
            #do promenne data jsou ulozena data z formulare (z formulare v promenne formular)
            data = formular.cleaned_data['predmet_form']
            #do slovniku pod klic predmet je ulozen pozadovany predmet z tridy predmet,
            #nebo je vracen kod 404 pokud predmet se zadanym nazvem neexistuje
            up['predmet'] = get_object_or_404(Predmet, zkratka = data)
            #ulozime do slovniku vsechny ucitele, kteri uci dany predmet
            up['ucitele'] = Ucitel.objects.filter(predmet = up['predmet'].id)
             
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = upForm({'predmet_form' : 'Zadej zkratku predmetu'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular
        formular = upForm({'predmet_form' : 'Zadej zkratku predmetu'})
           
    #ulozime do slovniku formular
    up['formular'] = formular
    #vraci slovnik 'up' do templatu ucitele_predmetu.html)
    return render_to_response('ucitele_predmetu.html', up)

def zkousky_predmetu(request):
    zp = {}
    zp.update(csrf(request))
    #podminka - pokud je neco poslano ve formulari, tak:
    if request.method == 'POST':
        #do formulare se ulozi co je poslano metodou POST
        formular = zpForm(request.POST)
        #pokud je formular vporadku - obsahuje data spravneho formatu, tak:...
        if formular.is_valid():
            #do promenne data jsou ulozena data z formulare (z formulare v promenne formular)
            data = formular.cleaned_data['predmet_form']
            #do slovniku pod klic predmet je ulozen pozadovany predmet z tridy predmet,
            #nebo je vracen kod 404 pokud predmet se zadanym nazvem neexistuje
            zp['predmet'] = get_object_or_404(Predmet, zkratka = data)
            #ulozime do slovniku vsechny zkousky, ktere dany predmet ma jako seznam
            zp['zkousky'] = [zp['predmet'].prvni_termin]
            zp['zkousky'] += [zp['predmet'].druhy_termin]
            zp['zkousky'] += [zp['predmet'].treti_termin]
        # pokud formular neni validni, pak budeme delat, jakoze nic nebylo zadano
        else:
            formular = zpForm({'predmet_form' : 'Zadej zkratku predmetu'})
    #pokud nic ve formulari metodou post neni poslano, pak se jedna pravdpodobne o 1. spusteni:    
    else:
        #nainicializujeme formular
        formular = zpForm({'predmet_form' : 'Zadej zkratku predmetu'})
           
    #ulozime do slovniku formular
    zp['formular'] = formular
    #vraci slovnik 'up' do templatu ucitele_predmetu.html)
    return render_to_response('zkousky_predmetu.html', zp)

def garanti(request):
    #vytahnu si vsechny predmety z db a seradim je podle abecedy
    pr = Predmet.objects.all().order_by('nazev')
    #vytahnu si vsechny ucitele a seradim je podle prijmeni
    uc = Ucitel.objects.all().order_by('prijmeni')
    #vracim slovnik se vsemi ucitely i predmety a zobrazim je podle templatu garanti.html
    return render_to_response('garanti.html', {'pr' : pr, 'uc' : uc}) 
