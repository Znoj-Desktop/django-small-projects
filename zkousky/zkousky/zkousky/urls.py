from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'zkousky.views.home', name='home'),
    # url(r'^zkousky/', include('zkousky.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^zkousky/', 'zkousky.models.views.zkousky'),
    url(r'^ucitele/', 'zkousky.models.views.ucitele'),
    url(r'^predmety/', 'zkousky.models.views.predmety'),
    url(r'^ucitele_predmetu/', 'zkousky.models.views.ucitele_predmetu'),
    url(r'^zkousky_predmetu/', 'zkousky.models.views.zkousky_predmetu'),
    url(r'^garanti/', 'zkousky.models.views.garanti'),
)
